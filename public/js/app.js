var sio = io();
var piece_color;
var game;
var board;
var all_games = {};

function updateBoard(fen) {
  game.load(fen);
  board.position(game.fen());
}

function joinGame() {
  sio.emit("new_game");
  $("#join_game_button").hide();
}

$("#watch").click(function() {
  watch = !watch;
  if (watch) {
    sio.emit("start_watching")
  } else {
    sio.emit("stop_watching")
  }
});

sio.on("user_count_changed", function(new_count) {
  $("#user_count").text(new_count);
});

sio.on("new_game", function(fen, color) {
  $("#board").show();
  updateBoard(fen);
  board.orientation(color);
  piece_color = color;
});

sio.on("opponent_move", function(fen) {
  updateBoard(fen);
});

sio.on("opponent_disconnect", function() {
  $("#join_game_button").show();
  $("#board").hide();
});

sio.on("msg", function(msg, time) {
  $("#msg_box").text(msg);
  if (time != 0) {
    setTimeout(function(){$("#msg_box").text("");}, time);
  }
});

sio.on("game_updates", function(updates) {
  Object.keys(updates).forEach(function(id) {
    if (!(id in all_games)) {
      $("#watched_games").append(
        '<div id="' + id + '" class="watched_board"></div>'
      );
      all_games[id] = new ChessBoard(id);
    }
    all_games[id].position(updates[id])
  });
});

sio.on("game_over", function(id) {
  delete all_games[id];
  $("#"+id).remove();

});

$(document).ready(function() {
  game = new Chess();
  board = new ChessBoard("board", {
    position: game.fen(),
    draggable: true,
    onDrop: function(from, to) {
      if (game.turn() != piece_color[0]) {
        return 'snapback';
      }
      let move = game.move({from: from, to: to});
      if (!move) {
        return 'snapback';
      }
      sio.emit('move_made', from, to, function(fen) {
        updateBoard(fen);
      });
    },
  });
  $("#board").hide();
})

